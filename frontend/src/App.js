import React from 'react';
import SignIn from './components/SignIn';
import {BrowserRouter, Link as RouterLink, Route, Switch,} from 'react-router-dom';
import NavBar from './components/NavBar';
import Home from './components/Home';
import CommissionManager from './components/CommisionManager';
import Container from '@material-ui/core/Container';
import {HOME, MAKE_COMMISSION, SIGN_IN} from './urls';
import {Box, Link} from '@material-ui/core';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import PrivateRoute from './components/PrivateRoute';
import CircularProgress from '@material-ui/core/CircularProgress';
import Backdrop from '@material-ui/core/Backdrop';
import {selectIsLoading} from './store/backdrop/selectors';
import {connect} from 'react-redux';
import MySnackbar from './components/Snakbar';

function App({isLoading}) {
  return <BrowserRouter>
    <Backdrop open={isLoading} style={{zIndex: 3000}}>
      <CircularProgress color="primary"/>
    </Backdrop>
    <MySnackbar/>
    <Switch>
      <Route path={SIGN_IN} component={SignIn}/>
      <PrivateRoute component={DefaultContainer}/>
    </Switch>
  </BrowserRouter>;
}

const mapStateToProps = state => ({isLoading: selectIsLoading(state)});
export default connect(mapStateToProps)(App);

function DefaultContainer() {
  return <Box>
    <NavBar/>
    <Container component={Box} my={2}>
      <Switch>
        <Route path={HOME} exact/>
        <Breadcrumbs aria-label="breadcrumb">
          <Link component={RouterLink} color="inherit" to={HOME}>
            <Box display='flex' my={2}>
              <ArrowBackIcon/>
              Назад
            </Box>
          </Link>
        </Breadcrumbs>
      </Switch>
      <Switch>
        <Route path={MAKE_COMMISSION} component={CommissionManager}/>
        <Route path={HOME} component={Home}/>
      </Switch>
    </Container>
  </Box>;
}
