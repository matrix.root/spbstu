import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import {SIGN_IN} from "../urls";
import {connect} from "react-redux";

function PrivateRoute({component: Component, token, ...rest}) {
    return (
        <Route {...rest} render={
            (props) => token ? <Component {...props} /> : <Redirect to={SIGN_IN}/>
        }/>
    );
}

const mapStateToProps = (state) => ({token: state.user.token});

export default connect(mapStateToProps)(PrivateRoute);
