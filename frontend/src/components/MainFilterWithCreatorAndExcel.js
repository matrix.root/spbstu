import React from 'react';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import CommissionCreatorWithExcel from './CommissionCreator';

export function MainFilterWithCreatorAndExcel(extra_fields) {
    const getTextField = makeTextFieldGetter({md: 4});
    return <Paper component={Box} display='flex' flexDirection="column" p={3}>
        <Grid component={Box} mb={3} container spacing={2}>
            <Grid item xs={12} sm={4}><Typography variant='h4'>Комиссии</Typography></Grid>
            <Grid item xs={12} sm={8}><CommissionCreatorWithExcel/></Grid>
        </Grid>
        <Grid container spacing={2}>
            {extra_fields.map(getTextField)}
        </Grid>
    </Paper>;
}

export function makeTextFieldGetter(grid_props = {}) {
    return function getTextField([label, {idToValue, currentID}, setID]) {
        let props = {label: label, key: label, value: idToValue[currentID] || ''};
        let menuItems = [];
        if (Object.keys(idToValue).length > 0) {
            props.select = true;
            for (let id in idToValue) {
                let value = idToValue[id];
                menuItems.push(
                    <MenuItem key={id} value={value} onClick={() => setID(id)}>
                        {value}
                    </MenuItem>,
                );
            }
        } else {
            props.disabled = true;
        }
        return getGridItem(
            <TextField fullWidth {...props} style={{width: '100%'}}>
                {menuItems}
            </TextField>,
            label,
        );
    };

    function getGridItem(item, key) {
        return <Grid item xs={12} sm={6} {...grid_props} key={key}>{item}</Grid>;
    }
}