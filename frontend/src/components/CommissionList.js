import Paper from '@material-ui/core/Paper';
import React from 'react';
import {selectCommissions} from '../store/commission_manager/commissions/selectors';
import {connect} from 'react-redux';
import Box from '@material-ui/core/Box';
import {selectTeachers} from '../store/commission_manager/teachers/selectors';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import StarIcon from '@material-ui/icons/Star';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Swal from 'sweetalert2';
import {deleteCommission} from '../store/commission_manager/commissions/actions';
import {openEditor} from '../store/commission_manager/editor/actions';

function CommissionList({commissions, teachers, deleteCommission, openEditor}) {
    return commissions.length < 1 ? <Typography variant='h2' component={Box} align='center' my={5}>Список коммиссий пуст</Typography> :
        <Grid component={Box} my={1} container spacing={1}>{commissions.map(
            commission => {
                const deleteMe = () => {
                    Swal.fire({
                        title: 'Вы уверены?',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonText: 'Да',
                        cancelButtonText: 'Нет',
                    }).then((result) => {
                        if (result.value) {
                            deleteCommission(commission.id);
                        }
                    });
                };
                const date = new Date(commission.date);
                const editMe = () => openEditor(commission);
                return <Grid item xs={12} sm={6} md={4} key={commission.id}>
                    <Paper component={Box} style={{height: '100%'}} p={1}
                           justifyContent='space-between' display='flex'
                           flexDirection='column'>
                        <Box>
                            <Box px={2} mt={1} display='flex' justifyContent='space-between'>
                                <Typography variant='h5'>
                                    {date.toLocaleDateString()}
                                </Typography>
                                <Typography variant='h5'>
                                    {date.toLocaleTimeString()}
                                </Typography>
                            </Box>
                            <List subheader={<ListSubheader>Преподаватели</ListSubheader>}>
                                {commission.teachers.map(
                                    teacher => <ListItem key={teacher.id}>
                                        {teacher.is_main && <ListItemIcon>
                                            <StarIcon/>
                                        </ListItemIcon>}
                                        <ListItemText inset={!teacher.is_main}
                                                      primary={teachers[teacher.id]}/>
                                    </ListItem>)}
                            </List>
                        </Box>
                        <ButtonGroup fullWidth color="primary" variant='text'>
                            <Button onClick={editMe}>Редактировать</Button>
                            <Button onClick={deleteMe}>Удалить</Button>
                        </ButtonGroup>
                    </Paper>
                </Grid>;
            },
        )}
        </Grid>;
}

const mapStateToProps = state => ({
    commissions: selectCommissions(state),
    teachers: selectTeachers(state),
});
export default connect(mapStateToProps, {deleteCommission, openEditor})(
    CommissionList);