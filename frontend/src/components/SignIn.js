import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import {SIGN_UP_PHOTO_URL} from '../settings';
import {Redirect} from 'react-router-dom';
import {HOME} from '../urls';
import {connect} from 'react-redux';
import {set_email, set_password, try_login} from '../store/auth/actions';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: `url(${SIGN_UP_PHOTO_URL})`,
    backgroundRepeat: 'no-repeat',
    backgroundColor:
        theme.palette.type === 'light'
            ? theme.palette.grey[50]
            : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(10, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.dark,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function SignIn({auth, token, set_email, set_password, try_login}) {
  const classes = useStyles();
  return token ? <Redirect to={HOME}/> : <>
    <Grid container component="main" className={classes.root}>
      <CssBaseline/>
      <Grid item xs={false} sm={4} md={7} className={classes.image}/>
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon/>
          </Avatar>
          <Typography component="h1" variant="h5">
            Войти
          </Typography>
          <form className={classes.form} noValidate>
            <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                label="Логин"
                onChange={(event => set_email(event.target.value))}
                value={auth.email}
                autoFocus
            />
            <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                label="Пароль"
                type="password"
                onChange={(event => set_password(event.target.value))}
                value={auth.password}
                autoComplete="current-password"
            />
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={(event) => {
                  event.preventDefault();
                  try_login();
                }}
            >
              Войти
            </Button>
          </form>
        </div>
      </Grid>
    </Grid>
  </>;
}

const mapStateToProps = (state) => ({auth: state.auth, token: state.user.token});
const mapDispatchToProps = {
  set_email,
  set_password,
  try_login,
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);