import Snackbar from '@material-ui/core/Snackbar';
import React from 'react';
import {selectSnackbar} from '../store/snackbar/selectors';
import {hide} from '../store/snackbar/actions';
import {connect} from 'react-redux';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function MySnackbar({params, hide}) {
  const {message} = params;
  return <Snackbar
      anchorOrigin={{vertical: 'bottom', horizontal: 'left'}}
      {...params} onClose={hide}>
    <Alert onClose={hide} severity={params.severity}>
      {message}
    </Alert>
  </Snackbar>;
}

const mapStateToProps = state => ({params: selectSnackbar(state)});
export default connect(mapStateToProps, {hide})(MySnackbar);