import DateFnsUtils from '@date-io/date-fns';
import Box from '@material-ui/core/Box';
import {
  KeyboardDatePicker,
  KeyboardTimePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import React from 'react';

function DatePicker({date, setDate}) {
  return <MuiPickersUtilsProvider utils={DateFnsUtils}>
    <Box>
      <KeyboardDatePicker
          fullWidth
          margin="normal"
          label="Выберите дату"
          format="MM/dd/yyyy"
          value={date}
          onChange={setDate}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
      />
      <KeyboardTimePicker
          margin="normal"
          fullWidth
          label="Выберите время"
          value={date}
          onChange={setDate}
          KeyboardButtonProps={{
            'aria-label': 'change time',
          }}
      />
    </Box>
  </MuiPickersUtilsProvider>;
}

export default DatePicker;