import Button from '@material-ui/core/Button';
import React, {useState} from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import DatePicker from './DatePicker';
import TeacherPicker from './TeacherPicker';
import Box from '@material-ui/core/Box';
import Confirmation from './ConfirmationWindow';
import {selectDate, selectTeachersIDs,} from '../../store/commission_manager/creator/selectors';
import {addTeacher, createCommission, removeTeacher, setDate,} from '../../store/commission_manager/creator/actions';
import {selectTeachers} from '../../store/commission_manager/teachers/selectors';
import {connect} from 'react-redux';
import ExcelTools from "../ExcelTools";
import Grid from "@material-ui/core/Grid";

function CommissionCreatorWithExcelTools({date, setDate, teachers, creatableTeacherIDs, addTeacher, removeTeacher, createCommission}) {
    const [isOpen, setOpen] = useState(false);
    let close = () => setOpen(false);
    return <>
        <Grid container spacing={1}>
            <Grid item xs={12} md={9}><ExcelTools/></Grid>
            <Grid item xs={12} md={3}><Button variant='contained' fullWidth onClick={() => setOpen(true)}>Создать</Button></Grid>
        </Grid>
        <Dialog open={isOpen} onClose={close}>
            <DialogTitle>Создание комиссии</DialogTitle>
            <DialogContent>
                <DatePicker date={date} setDate={setDate}/>
                <Box m={2}/>
                <TeacherPicker teachers={teachers}
                               editableTeacherIDs={creatableTeacherIDs}
                               addTeacher={addTeacher} removeTeacher={removeTeacher}/>
            </DialogContent>
            <DialogActions>
                <Button onClick={close}>
                    Отмена
                </Button>
                <Confirmation teachers={teachers}
                              creatableTeachers={creatableTeacherIDs}
                              action={createCommission} callback={close}/>
            </DialogActions>
        </Dialog>
    </>;
}

const mapStateToProps = state => ({
    date: selectDate(state),
    teachers: selectTeachers(state),
    creatableTeacherIDs: selectTeachersIDs(state),
});
const mapDispatchToPtops = {
    setDate,
    addTeacher,
    removeTeacher,
    createCommission,
};
export default connect(mapStateToProps, mapDispatchToPtops)(CommissionCreatorWithExcelTools);