import {FixedSizeList} from 'react-window';
import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Checkbox from '@material-ui/core/Checkbox';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';

function TeacherPicker(props) {
  const {teachers} = props;
  return <>
    <Typography>Выберите преподавателей</Typography>
    <FixedSizeList height={400} width='auto' itemSize={46}
                   itemCount={Object.keys(teachers).length}
                   itemData={props}>
      {renderRow}
    </FixedSizeList>
  </>;
}

export default TeacherPicker;

function renderRow({index, style, data}) {
  const {
    teachers, editableTeacherIDs, addTeacher,
    removeTeacher,
  } = data;
  const [id, teacher] = Object.entries(teachers)[index];
  const checked = editableTeacherIDs.includes(id);
  const handleClick = () => {
    const setValue = checked ? removeTeacher : addTeacher;
    setValue(id);
  };

  return (
      <ListItem button style={style} key={id} onClick={handleClick}>
        <ListItemIcon>
          <Checkbox
              edge="start"
              checked={checked}
              color='primary'
          />
        </ListItemIcon>
        <ListItemText primary={teacher}/>
      </ListItem>
  );
}