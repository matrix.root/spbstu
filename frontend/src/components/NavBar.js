import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import React from 'react';
import {makeStyles} from '@material-ui/styles';
import Button from '@material-ui/core/Button';
import {NAVBAR_LOGO_URL} from '../settings';
import Container from '@material-ui/core/Container';
import {connect} from 'react-redux';
import {clear_token} from '../store/user/actions';
import Swal from 'sweetalert2';

const useStyles = makeStyles((theme) => ({
    root: {
        justifyContent: 'space-between'
    },
    logo: {
        flexGrow: 1,
        maxWidth: '300px',
        [theme.breakpoints.down('sm')]: {
            maxWidth: '200px',
        },
    },
}));

function NavBar({clear_token}) {
    const classes = useStyles();
    const log_out = () => {
        Swal.fire({
            title: "Уже уходите?",
            icon: "question",
            showCancelButton: true,
            confirmButtonText: 'Да',
            cancelButtonText: "Нет, я случайно"
        }).then((result) => {
            if (result.value) {
                clear_token()
            }
        });
    }

    return <AppBar position="static" color='inherit'>
        <Container disableGutters={true}>
            <Toolbar className={classes.root}>
                <img className={classes.logo} src={NAVBAR_LOGO_URL} alt="Логотип"/>
                <Button variant="outlined" color="primary" onClick={log_out}>Выйти</Button>
            </Toolbar>
        </Container>
    </AppBar>
}

export default connect(null, {clear_token})(NavBar);