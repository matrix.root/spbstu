import axios from "axios";
import FileDownload from 'js-file-download'
import React, {useCallback, useMemo, useState} from "react";
import {useDropzone} from "react-dropzone";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Box from "@material-ui/core/Box";
import {connect} from "react-redux";
import {startLoading, stopLoading} from "../store/backdrop/actios";
import * as Swal from "sweetalert2";
import {loadCommissions} from "../store/commission_manager/commissions/actions";

const baseStyle = {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '20px',
    borderWidth: 2,
    borderRadius: 2,
    borderColor: '#eeeeee',
    borderStyle: 'dashed',
    backgroundColor: '#fafafa',
    color: '#bdbdbd',
    outline: 'none',
    transition: 'border .24s ease-in-out',
};

const activeStyle = {
    borderColor: '#2196f3'
};

const acceptStyle = {
    borderColor: '#00e676'
};

const rejectStyle = {
    borderColor: '#ff1744'
};


function download_excel() {
    axios.request({
        url: 'download/',
        method: 'GET',
        responseType: 'blob'
    }).then(({data}) => FileDownload(data, 'file.xlsx'))
}

function ExcelTools({startLoading, stopLoading, loadCommissions}) {
    const [isOpen, setIsOpen] = useState(false);
    const onDrop = useCallback(acceptedFiles => {
        startLoading();
        const formData = new FormData();
        formData.append('file', acceptedFiles[0]);
        try {
            axios.post('upload/', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then(r => loadCommissions());
        } catch (e) {
            Swal.fire(e.message);
        } finally {
            stopLoading();
            setIsOpen(false);
        }
    }, [])
    const {
        getRootProps, getInputProps, isDragActive,
        isDragAccept,
        isDragReject
    } = useDropzone({onDrop})
    const style = useMemo(() => ({
        ...baseStyle,
        ...(isDragActive ? activeStyle : {}),
        ...(isDragAccept ? acceptStyle : {}),
        ...(isDragReject ? rejectStyle : {})
    }), [
        isDragActive,
        isDragReject,
        isDragAccept
    ]);

    return <>
        <ButtonGroup variant='outlined' fullWidth>
            <Button onClick={download_excel}>Скачать Excel</Button>
            <Button onClick={() => setIsOpen(true)}>Загрузить Excel</Button>
        </ButtonGroup>
        <Dialog open={isOpen} onClose={() => setIsOpen(false)}>
            <DialogTitle>Загрузить Excel</DialogTitle>
            <Box m={2} {...getRootProps({style})}>
                <input {...getInputProps()} />
                <p>Перетащите файл сюда или нажмите для выбора файла</p>
            </Box>
        </Dialog>
    </>
}

export default connect(null, {startLoading, stopLoading, loadCommissions})(ExcelTools);