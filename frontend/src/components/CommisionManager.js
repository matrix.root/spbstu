import React, {useEffect} from 'react';
import {MainFilterWithCreatorAndExcel} from './MainFilterWithCreatorAndExcel';
import {connect} from 'react-redux';
import {selectField} from '../store/commission_manager/filter/selectors';
import {
  loadFilters,
  setFieldCurrentId,
} from '../store/commission_manager/filter/actions';
import {bindActionCreators} from 'redux';
import {
  CONTROL_TYPE,
  COURSE,
  DEPARTMENT,
  EDUCATION_FORM,
  INSTITUTE,
  SUBJECT,
} from '../store/commission_manager/filter/filter_fields';
import {loadTeachers} from '../store/commission_manager/teachers/actions';
import CommissionList from './CommissionList';
import CommissionEditor from './CommissionEditor';
import {EDITOR_FILTER, MAIN_FILTER} from '../store/commission_manager/settings';

export const labelsWithFields = [
  ['Институт', INSTITUTE],
  ['Департамент', DEPARTMENT],
  ['Курс', COURSE],
  ['Форма обучения', EDUCATION_FORM],
  ['Тип контроля', CONTROL_TYPE],
  ['Предмет', SUBJECT],
];

function CommissionChecker({fields, setters, loadFilters, loadTeachers}) {
  useEffect(() => {
    loadFilters(EDITOR_FILTER);
    loadFilters(MAIN_FILTER);
    loadTeachers();
  }, [loadFilters, loadTeachers]);

  const final_fields = [];
  for (let i in fields) {
    const field = fields[i];
    final_fields.push([
      field[0],
      field[1],
      setters[i],
    ]);
  }
  return <>
    {MainFilterWithCreatorAndExcel(final_fields)}
    <CommissionList/>
    <CommissionEditor/>
  </>;
}

const mapStateToProps = state => ({
  fields: labelsWithFields.map(
      ([label, field]) => [label, selectField(field)(state)]),
});

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators({loadFilters, loadTeachers}, dispatch),
  setters: bindActionCreators(
      labelsWithFields.map(obj => setFieldCurrentId(obj[1], MAIN_FILTER)),
      dispatch,
  ),
});

export default connect(mapStateToProps, mapDispatchToProps)(CommissionChecker);