import {red} from '@material-ui/core/colors';
import {createMuiTheme} from '@material-ui/core/styles';
import {responsiveFontSizes} from '@material-ui/core';

// A custom theme for this app
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#37B34A',
      contrastText: '#fff',
    },
    error: {
      main: red.A400,
    },
    type: process.env.REACT_APP_THEME_TYPE,
  },
});

export default responsiveFontSizes(theme);
