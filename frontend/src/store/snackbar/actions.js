export const SNACKBAR_SHOW_MESSAGE = 'SNACKBAR_SHOW_MESSAGE';
export const SNACKBAR_HIDE = 'SNACKBAR_HIDE';

export function showMessage(
    message, severity = 'success', autoHideDuration = 5,
) {
  autoHideDuration *= 1000;
  return {
    type: SNACKBAR_SHOW_MESSAGE, payload: {
      message, severity, autoHideDuration,
    },
  };
}

export function hide() {
  return {type: SNACKBAR_HIDE};
}