import {select} from '@redux-saga/core/effects';
import {call, put} from 'redux-saga/effects';
import axios from 'axios';
import {set_token} from '../user/actions';
import Swal from 'sweetalert2';
import {startLoading, stopLoading} from '../backdrop/actios';

export function* login_user() {
  const {auth} = yield select();
  yield put(startLoading());
  try {
    const response = yield call(
        () => axios.post('auth/token/login/', {
          username: auth.email,
          password: auth.password,
        }));
    yield put(set_token(response.data['auth_token']));
    yield put(stopLoading());
    yield call(() => Swal.fire({
      title: 'Поздравляем!',
      text: 'Вы успешно вошли в систему!',
      icon: 'success',
      button: 'Ура!',
    }));
  } catch (e) {
    yield put(stopLoading());
    yield call(
        () => Swal.fire('Упс!', 'Проверьте свой логин и пароль', 'error'));
  } finally {
  }
}