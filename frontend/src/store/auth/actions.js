export const AUTH_SET_EMAIL = 'AUTH_SET_EMAIL';
export const AUTH_SET_PASSWORD = 'AUTH_SET_PASSWORD';
export const AUTH_TRY_LOGIN = 'AUTH_TRY_LOGIN';

export function set_email(email) {
  return {
    type: AUTH_SET_EMAIL,
    payload: email,
  };
}

export function set_password(password) {
  return {
    type: AUTH_SET_PASSWORD,
    payload: password,
  };
}

export function try_login() {
  return {
    type: AUTH_TRY_LOGIN,
  };
}
