export const SUBJECT = 'subject';
export const INSTITUTE = 'institute';
export const DEPARTMENT = 'department';
export const COURSE = 'course';
export const EDUCATION_FORM = 'education_form';
export const CONTROL_TYPE = 'control_type';