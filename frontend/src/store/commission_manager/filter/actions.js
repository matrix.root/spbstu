import {selectFieldCurrentId as selectFieldCurrentIdEditor} from '../editor/selectors';
import {selectFieldCurrentId} from './selectors';
import {EDITOR_FILTER, MAIN_FILTER} from '../settings';

export const COMMISSION_FILTER_LOAD_DATA = 'COMMISSION_FILTER_LOAD_DATA';
export const COMMISSION_FILTER_SET_FIELD_CURRENT_ID = 'COMMISSION_FILTER_SET_FIELD_CURRENT_ID';
export const COMMISSION_FILTER_SET_FIELD_ID_TO_VALUE = 'COMMISSION_FILTER_SET_FIELD_ID_TO_VALUE';

export function loadFilters(name) {
  return {type: COMMISSION_FILTER_LOAD_DATA, name};
}

const nameToSelector = {
  [MAIN_FILTER]: selectFieldCurrentId,
  [EDITOR_FILTER]: selectFieldCurrentIdEditor,
};

export function setFieldCurrentId(field, name, isInitial = false) {
  return function(id) {
    return {
      name,
      type: COMMISSION_FILTER_SET_FIELD_CURRENT_ID,
      field: field,
      payload: id,
      fieldSelector: nameToSelector[name],
      isInitial,
    };
  };
}

export function setFieldIdToValue(field, id_to_value, name, isInitial) {
  return {
    type: COMMISSION_FILTER_SET_FIELD_ID_TO_VALUE,
    field: field,
    payload: id_to_value,
    name,
    isInitial,
  };
}