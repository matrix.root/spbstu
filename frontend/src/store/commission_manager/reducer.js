import {combineReducers} from 'redux';
import {createNamedFilterReducer} from './filter/reducer';
import teacherReducer from './teachers/reducer';
import creatorReducer from './creator/reducer';
import {commissionsReducer} from './commissions/reducer';
import editorReducer from './editor/reducer';
import {MAIN_FILTER} from './settings';

export const commissionManagerReducer = combineReducers({
  filter: createNamedFilterReducer(MAIN_FILTER),
  teachers: teacherReducer,
  creator: creatorReducer,
  commissions: commissionsReducer,
  editor: editorReducer,
});