import {TEACHERS_SET} from './actions';

const TEACHERS = 'TEACHERS';

const initialState = JSON.parse(localStorage.getItem(TEACHERS)) || {};

export default function teacherReducer(state = initialState, action) {
    switch (action.type) {
        case TEACHERS_SET:
            localStorage.setItem(TEACHERS, JSON.stringify(action.payload))
            return action.payload;
        default:
            return state;
    }
}