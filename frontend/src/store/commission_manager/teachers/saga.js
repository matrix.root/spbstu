import {call} from 'redux-saga/effects';
import axios from 'axios';
import {put, takeEvery} from '@redux-saga/core/effects';
import {setTeachers, TEACHERS_LOAD} from './actions';

export function* teachersSaga() {
  yield takeEvery(TEACHERS_LOAD, loadTeachers);
}

export function* loadTeachers() {
  const teachers = yield call(axios.get, 'teachers');
  yield put(setTeachers(teachers.data.reduce(
      (obj, teacher) => {
        obj[teacher.id.toString()] = teacher.short_name;
        return obj;
      }, {},
  )));
}