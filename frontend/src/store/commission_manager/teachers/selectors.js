import {createSelector} from 'reselect';
import {selectCommissionManager} from '../selectors';

export const selectTeachers = createSelector(selectCommissionManager,
    manager => manager.teachers,
);