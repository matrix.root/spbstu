import {createSelector} from 'reselect';
import {selectCommissionManager} from '../selectors';

export const selectCommissions = createSelector(selectCommissionManager,
    manager => manager.commissions);