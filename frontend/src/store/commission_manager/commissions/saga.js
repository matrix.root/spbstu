import {call} from 'redux-saga/effects';
import axios from 'axios';
import {put, select} from '@redux-saga/core/effects';
import {showMessage} from '../../snackbar/actions';
import {loadCommissions as loadAction, setCommissions} from './actions';
import {selectFilterParams} from '../filter/selectors';

export function* loadCommissions() {
  try {
    const params = yield select(selectFilterParams);
    const commissions = yield call(axios.get, 'commissions',
        {params},
    );
    yield put(setCommissions(commissions.data));
  } catch (e) {
    yield put(showMessage('Проблемы с загрузкой комиссий', 'error'));
  }
}

export function* deleteCommission({payload}) {
  try {
    yield call(axios.delete, `commissions/${payload}/`);
    yield put(loadAction());
    yield put(showMessage('Успешно удалено', 'success'));
  } catch (e) {
    yield put(showMessage('Что-то пошло не так...', 'error'));
  }
}