import {COMMISSIONS_ADD, COMMISSIONS_SET} from './actions';

const COMMISSIONS = 'COMMISSIONS';
const initialState = JSON.parse(localStorage.getItem(COMMISSIONS)) || [];

export function commissionsReducer(state = initialState, action) {
    switch (action.type) {
        case COMMISSIONS_ADD:
            return [action.payload].concat(state);
        case COMMISSIONS_SET:
            localStorage.setItem(COMMISSIONS, JSON.stringify(action.payload))
            return action.payload;
        default:
            return state;
    }
}