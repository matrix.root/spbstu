import {all, takeLatest} from '@redux-saga/core/effects';
import {filterSaga} from './filter/saga';
import {teachersSaga} from './teachers/saga';
import {creatorSaga} from './creator/saga';
import {deleteCommission, loadCommissions} from './commissions/saga';
import {COMMISSION_FILTER_SET_FIELD_CURRENT_ID} from './filter/actions';
import {COMMISSIONS_DELETE, COMMISSIONS_LOAD} from './commissions/actions';
import {editCurrent, setEditorFilters} from './editor/saga';
import {EDITOR_EDIT_CURRENT, EDITOR_OPEN} from './editor/actions';

export function* commissionManagerSaga() {
  yield all([
    teachersSaga(),
    filterSaga(),
    creatorSaga(),
    takeLatest(COMMISSION_FILTER_SET_FIELD_CURRENT_ID, loadCommissions),
    takeLatest(COMMISSIONS_DELETE, deleteCommission),
    takeLatest(COMMISSIONS_LOAD, loadCommissions),
    takeLatest(EDITOR_OPEN, setEditorFilters),
    takeLatest(EDITOR_EDIT_CURRENT, editCurrent),
  ]);
}