import {
  EDITOR_ADD_TEACHER,
  EDITOR_CLOSE,
  EDITOR_OPEN,
  EDITOR_REMOVE_TEACHER,
  EDITOR_SET_DATE,
  EDITOR_SET_FIELD_CURRENT_ID,
  EDITOR_SET_NEXT_STAGE,
  EDITOR_SET_PREV_STAGE,
} from './actions';
import {combineReducers} from 'redux';
import {createNamedFilterReducer} from '../filter/reducer';
import {EDITOR_FILTER} from '../settings';

const initialState = {
  isOpen: false,
  commission: {},
  editableTeacherIDs: [],
  stage: 0,
};

function editorReducer(state = initialState, action) {
  let oldFieldState;
  switch (action.type) {
    case EDITOR_OPEN:
      const commission = action.payload;
      const teachers = commission.teachers || [];
      return {
        ...state,
        isOpen: true,
        commission: commission,
        editableTeacherIDs: teachers.map(teacher => teacher.id.toString()),
        stage: 0
      };
    case EDITOR_CLOSE:
      return {...state, isOpen: false};
    case EDITOR_SET_DATE:
      return {
        ...state,
        commission: {...state.commission, date: action.payload},
      };
    case EDITOR_ADD_TEACHER:
      return {
        ...state,
        editableTeacherIDs: state.editableTeacherIDs.concat(action.payload),
      };
    case EDITOR_REMOVE_TEACHER:
      return {
        ...state,
        editableTeacherIDs: state.editableTeacherIDs.filter(
            id => id !== action.payload),
      };
    case EDITOR_SET_NEXT_STAGE:
      return {...state, stage: state.stage + 1};
    case EDITOR_SET_PREV_STAGE:
      return {...state, stage: state.stage - 1};
    case EDITOR_SET_FIELD_CURRENT_ID:
      oldFieldState = state.commission[action.field];
      return {
        ...state,
        commission: {...state.commission, [action.field]: action.payload},
      };
    default:
      return state;
  }
}

export default combineReducers({
  editor: editorReducer,
  filters: createNamedFilterReducer(EDITOR_FILTER),
});