import {createSelector} from 'reselect';
import {selectCommissionManager} from '../selectors';
import {
  CONTROL_TYPE,
  COURSE,
  DEPARTMENT,
  EDUCATION_FORM, SUBJECT,
} from '../filter/filter_fields';

export const selectEditorReducer = createSelector(selectCommissionManager,
    manager => manager.editor);

export const selectEditor = createSelector(selectEditorReducer,
    reducer => reducer.editor);

export const selectCommission = createSelector(selectEditor,
    editor => editor.commission);

export const selectFilter = createSelector(
    selectEditorReducer, editor => editor.filters
);

export const selectField = fieldName => createSelector(selectFilter,
    filter => filter[fieldName]);

export const selectFieldCurrentId = (fieldName) => createSelector(
    selectField(fieldName), field => field.currentID,
);

export const selectFilterParams = createSelector(selectFilter,
    filter => Object.entries({
      department: DEPARTMENT,
      year: COURSE,
      education_form: EDUCATION_FORM,
      control_type: CONTROL_TYPE,
      subject: SUBJECT,
    }).reduce((obj, [param, field_name]) => {
      obj[param] = filter[field_name].currentID;
      return obj;
    }, {}));