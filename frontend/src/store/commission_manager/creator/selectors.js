import { createSelector } from 'reselect'
import { selectCommissionManager } from '../selectors'

export const selectCreator = createSelector(selectCommissionManager,
  manager => manager.creator,
)

export const selectDate = createSelector(selectCreator,
  creator => creator.date,
)

export const selectTeachersIDs = createSelector(selectCreator,
  creator => creator.teacher_ids)
