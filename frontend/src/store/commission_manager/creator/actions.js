export const COMMISSION_CREATOR_SET_DATE = 'COMMISSION_CREATOR_SET_DATE'
export const COMMISSION_CREATOR_ADD_TEACHER = 'COMMISSION_CREATOR_ADD_TEACHER'
export const COMMISSION_CREATOR_REMOVE_TEACHER = 'COMMISSION_CREATOR_REMOVE_TEACHER'
export const COMMISSION_CREATOR_CREATE = 'COMMISSION_CREATOR_CREATE'

export function setDate (payload) {
  return { type: COMMISSION_CREATOR_SET_DATE, payload }
}

export const addTeacher = (payload) => ({
  type: COMMISSION_CREATOR_ADD_TEACHER,
  payload,
})

export function removeTeacher (id) {
  return { type: COMMISSION_CREATOR_REMOVE_TEACHER, payload: id }
}

export function createCommission (head_id, close) {
  return { type: COMMISSION_CREATOR_CREATE, payload: head_id, close }
}