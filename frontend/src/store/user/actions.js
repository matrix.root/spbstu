export const USER_SET_TOKEN = 'USER_SET_TOKEN';
export const USER_CLEAR_TOKEN = 'USER_CLEAR_TOKEN';

export function set_token(token) {
    return {
        type: USER_SET_TOKEN,
        payload: token
    }
}

export function clear_token() {
    return {type: USER_CLEAR_TOKEN}
}