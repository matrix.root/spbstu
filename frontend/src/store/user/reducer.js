import {USER_TOKEN_KEY} from "../../settings";
import {USER_CLEAR_TOKEN, USER_SET_TOKEN} from "./actions";

const initialState = {
    token: localStorage.getItem(USER_TOKEN_KEY),
}

export default function user(state = initialState, action) {
    switch (action.type) {
        case USER_SET_TOKEN:
            localStorage.setItem(USER_TOKEN_KEY, action.payload);
            return {...state, token: action.payload}
        case USER_CLEAR_TOKEN:
            localStorage.removeItem(USER_TOKEN_KEY);
            return {...state, token: ''}
        default:
            return state
    }
}