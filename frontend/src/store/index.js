import {applyMiddleware, combineReducers, createStore} from 'redux';
import authReducer from './auth/reducer';
import userReducer from './user/reducer';
import {composeWithDevTools} from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import {mySaga} from './saga';
import backdropReducer from './backdrop/reducer';
import {commissionManagerReducer} from './commission_manager/reducer';
import snackbarReducer from './snackbar/reducer';
import editorReducer from './commission_manager/editor/reducer';

const sagaMiddleware = createSagaMiddleware();

const rootReducer = combineReducers({
  backdrop: backdropReducer,
  auth: authReducer,
  user: userReducer,
  commissionManager: commissionManagerReducer,
  snackbar: snackbarReducer,
});

export default createStore(rootReducer, composeWithDevTools(
    applyMiddleware(sagaMiddleware),
));

sagaMiddleware.run(mySaga);
