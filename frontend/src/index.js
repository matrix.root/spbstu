
import React from 'react';
import ReactDOM from 'react-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import {ThemeProvider} from '@material-ui/core/styles';
import App from './App';
import theme from './theme';
import {Provider} from "react-redux";
import store from "./store";
import axios from "axios";
import {API_BASE, USER_TOKEN_KEY} from "./settings";


axios.interceptors.request.use(function (config) {
    const token = localStorage.getItem(USER_TOKEN_KEY);
    if (token) {
        config.headers['Authorization'] = 'Token ' + token;
    }
    config.baseURL = API_BASE;
    return config;
}, function (error) {
    return Promise.reject(error);
});


ReactDOM.render(
    <Provider store={store}>
        <ThemeProvider theme={theme}>
            {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
            <CssBaseline/>
            <App/>
        </ThemeProvider>
    </Provider>,
    document.querySelector('#root'),
);
