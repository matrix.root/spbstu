from rest_framework import serializers

from spbstu.models import (
    Commission,
    CommissionTeacher,
    ControlType,
    Department,
    EducationForm,
    Institute,
    Subject,
    Teacher,
    Year
)


class EducationFormSerializer(serializers.ModelSerializer):
    class Meta:
        model = EducationForm
        fields = '__all__'


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = '__all__'


class TeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Teacher
        fields = '__all__'


class YearSerializer(serializers.ModelSerializer):
    course = serializers.ReadOnlyField()

    class Meta:
        model = Year
        fields = '__all__'


class InstituteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Institute
        fields = '__all__'


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = '__all__'


class ControlTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ControlType
        fields = '__all__'


class CommissionTeacherSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source='teacher_id')

    class Meta:
        model = CommissionTeacher
        fields = ['id', 'is_main']


class CommissionSerializer(serializers.ModelSerializer):
    teachers = CommissionTeacherSerializer(many=True, source='commissionteacher_set')

    def create(self, validated_data):
        teachers = validated_data.pop('commissionteacher_set')
        commission = super().create(validated_data)
        self.create_teachers(commission, teachers)
        return commission

    def update(self, commission, validated_data):
        teachers = validated_data.pop('commissionteacher_set')
        super().update(commission, validated_data)
        CommissionTeacher.objects.filter(commission_id=commission.id).delete()
        self.create_teachers(commission, teachers)
        return commission

    @staticmethod
    def create_teachers(commission, teachers):
        CommissionTeacher.objects.bulk_create(CommissionTeacher(**data, commission=commission) for data in teachers)

    class Meta:
        model = Commission
        fields = '__all__'
