import tempfile
from datetime import datetime

import pandas as pd
from django.db import transaction
from django.http import HttpResponse
from rest_framework import views, viewsets, status
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response

from spbstu.models import (
    Commission,
    ControlType,
    Department,
    EducationForm,
    Institute,
    Subject,
    Teacher,
    Year, CommissionTeacher
)
from spbstu.serializers import (
    CommissionSerializer,
    ControlTypeSerializer,
    DepartmentSerializer,
    EducationFormSerializer,
    InstituteSerializer,
    SubjectSerializer,
    TeacherSerializer,
    YearSerializer
)

COURSE = 'Курс'
FORM = 'Форма обучения'
SUBJECT = 'Дисциплина'
TYPE = 'Тип контроля'
DATE_FORMAT = '%d.%m.%Y'
DATE = 'Дата пороведения'
HEAD_TEACHER = 'Председатель коммиссии'
TEACHER = 'Член коммисси'


class InstituteViewSet(viewsets.ModelViewSet):
    queryset = Institute.objects.all()
    serializer_class = InstituteSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        if department_id := self.request.query_params.get('department'):
            dep = Department.objects.get(id=department_id)
            queryset = queryset.filter(id=dep.institute_id)
        return queryset


class DepartmentViewSet(viewsets.ModelViewSet):
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        if institute_id := self.request.query_params.get('institute_id'):
            queryset = queryset.filter(institute_id=institute_id)
        return queryset


class EducationFormViewSet(viewsets.ModelViewSet):
    queryset = EducationForm.objects.all()
    serializer_class = EducationFormSerializer


class SubjectViewSet(viewsets.ModelViewSet):
    queryset = Subject.objects.order_by('name')
    serializer_class = SubjectSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        if year_id := self.request.query_params.get('year_id'):
            queryset = queryset.filter(scheduleitem__group__year_id=year_id)
        if department_id := self.request.query_params.get('department_id'):
            queryset = queryset.filter(scheduleitem__group__department_id=department_id)
        return queryset


class TeacherViewSet(viewsets.ModelViewSet):
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer


class YearViewSet(viewsets.ModelViewSet):
    queryset = Year.objects.order_by('since_date')
    serializer_class = YearSerializer


class ControlTypeViewSet(viewsets.ModelViewSet):
    queryset = ControlType.objects.all()
    serializer_class = ControlTypeSerializer


class CommissionViewSet(viewsets.ModelViewSet):
    queryset = Commission.objects.all()
    serializer_class = CommissionSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        if control_type := self.request.query_params.get('control_type'):
            queryset = queryset.filter(control_type_id=control_type)
        if department := self.request.query_params.get('department'):
            queryset = queryset.filter(department_id=department)
        if education_form := self.request.query_params.get('education_form'):
            queryset = queryset.filter(education_form_id=education_form)
        if subject := self.request.query_params.get('subject'):
            queryset = queryset.filter(subject_id=subject)
        if year := self.request.query_params.get('year'):
            queryset = queryset.filter(year_id=year)
        return queryset


class DownloadExcelAPIView(views.APIView):
    def get(self, request):
        with tempfile.TemporaryFile() as tmp:
            df = pd.DataFrame(self.get_commissions())
            df.to_excel(tmp)
            tmp.seek(0)
            response = HttpResponse(tmp, content_type='application/vnd.ms-excel')
            response['Content-Disposition'] = 'attachment; filename=file.xlsx'
            return response

    def get_commissions(self):
        commissions = []
        for commission in Commission.objects.all():
            c_teachers = commission.commissionteacher_set.all()
            data = {
                COURSE: commission.year.course,
                FORM: commission.education_form.name,
                SUBJECT: commission.subject.name,
                TYPE: commission.control_type.name,
                DATE: commission.date.strftime(DATE_FORMAT),
                HEAD_TEACHER: str(next(filter(self.is_main, c_teachers))),
            }
            for i, c_teacher in enumerate(filter(lambda t: not self.is_main(t), c_teachers)):
                data[f'{TEACHER} {i + 1}'] = str(c_teacher)
            commissions.append(data)
        return commissions

    @staticmethod
    def is_main(teacher):
        return teacher.is_main


class UploadExcelAPIView(views.APIView):
    parser_classes = [MultiPartParser]

    @transaction.atomic
    def post(self, request, format=None):
        file_obj = request.data['file']
        df = pd.read_excel(file_obj, index_col=None)
        Commission.objects.all().delete()
        years = Year.objects.all()
        forms = EducationForm.objects.all()
        control_types = ControlType.objects.all()
        teachers = Teacher.objects.all()
        for i, row in df.iterrows():
            commission = Commission()
            commission.year = next(filter(lambda y: y.course == row[COURSE], years))
            commission.education_form = next(filter(lambda f: f.name == row[FORM], forms))
            subjects = Subject.objects.filter(scheduleitem__group__year=commission.year)
            commission.subject = next(filter(lambda s: s.name == row[SUBJECT], subjects))
            commission.control_type = next(filter(lambda t: t.name == row[TYPE], control_types))
            commission.date = datetime.strptime(row[DATE], DATE_FORMAT)
            schedule_item = commission.subject.scheduleitem_set.filter(group__year=commission.year).first()
            commission.department = schedule_item.group.department
            commission.save()
            CommissionTeacher.objects.create(
                teacher=next(filter(lambda t: t.short_name == row[HEAD_TEACHER], teachers)),
                is_main=True,
                commission=commission
            )
            for column in df:
                if column.startswith(TEACHER) and (teacher := row[column]):
                    CommissionTeacher.objects.create(
                        teacher=next(filter(lambda t: t.short_name == teacher, teachers)),
                        is_main=False,
                        commission=commission,
                    )
        return Response(status=status.HTTP_201_CREATED)
