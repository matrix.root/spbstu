from datetime import datetime

from django.db import models


class People(models.Model):
    first_name = models.CharField(max_length=128)
    patronymic = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    class Meta:
        abstract = True


class Institute(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class Department(models.Model):
    institute = models.ForeignKey(Institute, on_delete=models.PROTECT)
    name = models.CharField(max_length=128)
    base_num = models.CharField(max_length=128)
    ext_num = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class Year(models.Model):
    since_date = models.DateField()

    @property
    def course(self):
        now = datetime.now()
        years = now.year - self.since_date.year
        if now.month < 9:
            years -= 1
        return years + 1


class Group(models.Model):
    department = models.ForeignKey(Department, on_delete=models.PROTECT)
    number = models.CharField(max_length=128)
    year = models.ForeignKey(Year, on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.year}'


class EducationForm(models.Model):
    # очное/заочное
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class Status(models.Model):
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.name


class Student(People):
    GENDER_CHOICES = (
        (0, 'Male'),
        (1, 'Female'),
    )
    gender = models.IntegerField(choices=GENDER_CHOICES)
    birth_date = models.DateField()

    record_number = models.CharField(max_length=64)
    group = models.ForeignKey(Group, models.PROTECT)
    is_contract = models.BooleanField(default=False)
    education_form = models.ForeignKey(EducationForm, models.PROTECT)
    status = models.ForeignKey(Status, models.PROTECT)
    nationality = models.CharField(max_length=128)
    document = models.CharField(max_length=128)  # паспорт/не паспорт
    document_number = models.CharField(max_length=128)


class Teacher(People):
    short_name = models.CharField(max_length=128)
    position = models.CharField(max_length=128)  # Доцент, Ст. преп.
    financing_source = models.CharField(max_length=128)  # Бюд, В/Б
    rate = models.FloatField()  # 0,5; 1,25


class Subject(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class ClassRoom(models.Model):
    name = models.CharField(max_length=128)


class ScheduleItem(models.Model):
    week_number = models.IntegerField()
    time = models.TimeField()
    class_room = models.ForeignKey(ClassRoom, on_delete=models.PROTECT)
    subject = models.ForeignKey(Subject, on_delete=models.PROTECT)
    teacher = models.ForeignKey(Teacher, on_delete=models.PROTECT)
    group = models.ForeignKey(Group, on_delete=models.PROTECT)
    type = models.CharField(max_length=128)
    day = models.CharField(max_length=128)


class ControlType(models.Model):
    name = models.CharField(max_length=128)


class Commission(models.Model):
    teachers = models.ManyToManyField(Teacher, through='CommissionTeacher')
    department = models.ForeignKey(Department, on_delete=models.PROTECT)
    year = models.ForeignKey(Year, on_delete=models.PROTECT)
    education_form = models.ForeignKey(EducationForm, on_delete=models.PROTECT)
    control_type = models.ForeignKey(ControlType, on_delete=models.PROTECT)
    subject = models.ForeignKey(Subject, on_delete=models.PROTECT)
    date = models.DateTimeField()


class CommissionTeacher(models.Model):
    commission = models.ForeignKey(Commission, on_delete=models.CASCADE)
    teacher = models.ForeignKey(Teacher, on_delete=models.PROTECT)
    is_main = models.BooleanField(default=False)

    def __str__(self):
        return self.teacher.short_name
