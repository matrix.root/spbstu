python-dotenv~=0.13.0

Django~=3.0.6
djoser~=2.0.3
djangorestframework~=3.11.0
django-cors-headers~=3.2.1
psycopg2-binary~=2.8.5

pandas~=1.0.3
xlrd~=1.2.0
openpyxl~=3.0.3